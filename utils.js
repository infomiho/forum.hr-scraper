const PromisePool = require('es6-promise-pool')
const CONCURENT_PROMISES = 4

module.exports.serial = funcs =>
	funcs.reduce((promise, func) =>
		promise.then(result => func().then(Array.prototype.concat.bind(result))), Promise.resolve([]))

module.exports.promisePool = (promises) => {
	let i = 0
	const promiseProducer = () => {
		if (i === promises.length) return null
		const promise = promises[i]
		i = i + 1
		return promise()
	}

	const pool = new PromisePool(promiseProducer, CONCURENT_PROMISES)

	return pool.start()
}
