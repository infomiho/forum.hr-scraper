# Forum.hr scrapper

The scraper takes a sub-forum link as its argument and then iterates over that sub-forum's
pages finding threads. It then iterated over pages of the threads and gathers posts.

The output is a collection of JSON files formatted like this:

```
(Thread)
	-> {
		link, title, postCount,
		[Array of Post
			-> (Post) -> {
				(Author) -> {username, id}, content, id, date, timestamp
				}]
	}
```

## Run locally

1. Install Node.js ([Installation](https://nodejs.org/en/download/package-manager/))
2. Clone the repository & navigate to it
3. Run `npm install`
4. Run `node index.js`
5. The result will be located in the `data.json` file
