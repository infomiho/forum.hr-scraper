const request = require('request')
const rp = require('request-promise')
const cheerio = require('cheerio')
const resolvePath = require('path').resolve
const fs = require('fs')
const prompt = require('prompt')
const promisePool = require('./utils').promisePool
const iconv = require('iconv-lite')
const fecha = require('fecha')
const colors = require('colors')

const USERNAME_HIDDEN = '\'data-cfemail\''

let YESTERDAY = new Date()
YESTERDAY.setDate(YESTERDAY.getDate() - 1)
YESTERDAY = fecha.format(YESTERDAY, 'DD.MM.YYYY.')

let TODAY = new Date()
TODAY = fecha.format(TODAY, 'DD.MM.YYYY.')

let startTimestamp = null

prompt.start()

let rootSubForum = null
let subForumPageLimit = null
let threadPageLimit = null

const pages = []
let threads = []

prompt.get([{
		name: 'rootSubForum',
		required: true,
		description: 'Unesite link do podforuma s kojeg želite postove'
	},
	{
		name: 'subForumPageLimit',
		default: 10,
		required: true,
		type: 'integer',
		description: 'Koliko maksimalno stranica želite s odabranog podforuma'
	},
	{
		name: 'threadPageLimit',
		default: 100,
		required: false,
		description: 'Koliko maksimalno stranica želite sa svake teme'
	}
], function (err, result) {

	// const root = 'http://www.forum.hr/forumdisplay.php?f=8&order=desc'

	if (!err) {
		rootSubForum = result.rootSubForum
		subForumPageLimit = result.subForumPageLimit
		threadPageLimit = result.threadPageLimit

		init()
	} else {
		console.error('Došlo je do greške.'.red)
	}

})

let totalPostCount = 0
let totalCharCount = 0

const defaultTransform = (body) => {
	return cheerio.load(
		iconv.decode(new Buffer(body), "windows1250")
	)
}

const getThreads = ({
	link,
	page
}) => {

	console.log(`Getting thread links: page ${page}`.green)

	return new Promise((resolve, reject) => {
		rp({
				uri: link,
				encoding: null,
				transform: defaultTransform
			})
			.then(function ($) {

				const threads = []
				$('[id^=thread_title]').each((index, thread) => {
					threads.push({
						link: `http://www.forum.hr/${$(thread).attr('href')}`,
						posts: []
					})
				})

				resolve(threads)
			})
			.catch(error => {
				reject(error)
			})
	})
}

const getPosts = ({
	link,
	page
}) => {

	return new Promise((resolve, reject) => {
		rp({
				uri: link,
				encoding: null,
				transform: defaultTransform
			})
			.then(function ($) {

				const title = $('.tborder .navbar strong').text().trim()

				console.log(`Getting thread ${title} posts: page ${page}`.pink)

				const data = {
					posts: [],
					title
				}
				$('[id^="edit"]').each((index, post) => {

					const id = $(post).find('table[id^="post"]').first().attr('id')

					totalPostCount++

					const content = $(post).find('[id^="post_message"]').clone() //clone the element
						.children() //select all the children
						.remove('div') //remove all the children
						.end() //again go back to selected element
						.text()
						.trim()
						.replace(/\n{2,}/g, "\n")

					const quotes = []

					const quoteMatches = $(post).html().match(/p\=[0-9]+\#(post[0-9]+)/g)
					if (quoteMatches !== null) {
						quoteMatches.forEach(match => {
							quotes.push(match.split('#')[1])
						})
					}

					totalCharCount += content.length

					const date = $(post).find('.thead')
					.first()
					.text()
					.trim()
					.replace('Jučer', YESTERDAY)
					.replace('Danas', TODAY)

					const fechaParse = fecha.parse(date, 'DD.MM.YYYY., HH:mm')
					const timestamp = fechaParse !== null ? Math.floor(
						fechaParse.getTime() / 1000
					) : null

					const userInfo = $(post).find('.post_user_info > div').first()
					const author = {
						username: null,
						id: null
					}

					const processUsername = (username) => {
						return username.indexOf(USERNAME_HIDDEN) === -1 ?
							username :
							null
					}

					if ($(userInfo).find('[href^="member"]').length) {
						author.username = processUsername($(userInfo).find('[href^="member"]').text().trim())

						const href = $(userInfo).find('[href^="member"]').attr('href')
						author.id = typeof href !== 'undefined' ?
							href.match(/u=(\d+)/)[1] || null :
							null
					} else {
						author.username = processUsername($(userInfo).text().trim())
					}
					data.posts.push({
						author,
						content,
						id,
						date,
						timestamp,
						quotes
					})
				})
				resolve(data)
			})
			.catch(error => {
				reject(error)
			})
	})
}

const processThread = (threadIndex) => {

	console.log(`### Processing posts for thread #${threadIndex + 1} out of ${threads.length} ###`.yellow)

	const thread = threads[threadIndex]
	rp({
		uri: thread.link,
		transform: defaultTransform
	}).then(($) => {
		const threadPageMatch = $('.pagenav').first().find('td').first().text().match(/od (\d+)/)

		let threadPageCount = 1

		if (threadPageMatch !== null && threadPageMatch.length === 2) {
			threadPageCount = Math.min(
				threadPageLimit,
				parseInt(threadPageMatch[1], 10)
			)
		}

		const threadPages = []

		// Fill the pages array
		for (let i = 1; i <= threadPageCount; i++) {
			threadPages.push({
				page: i,
				link: `${thread.link}&page=${i}`
			})
		}

		const threadPagesPromises = threadPages.map(page =>
			() => getPosts(page).then(postsPart => {
				thread.posts = thread.posts.concat(postsPart.posts)
				thread.title = postsPart.title
				saveThread(thread, threadIndex)
			})
		)

		promisePool(threadPagesPromises).then(() => {
			// All posts finished
			if (threadIndex + 1 < threads.length) {
				processThread(threadIndex + 1)
			} else {
				done()
			}
		})

	})
}

const saveThread = (thread, index) => {
	if (thread.title) {
		const slug = `${index}-` + thread.title.toLowerCase()
			.replace(/\s+/g, '-')
			.replace(/[^\w\-]+/g, '')
			.replace(/\-\-+/g, '-')
			.replace(/^-+/, '')
			.replace(/-+$/, '')
		thread.postCount = thread.posts.length
		fs.writeFile(`./data/${slug}.json`, JSON.stringify(thread), function (err) {
			if (err) return console.log(err.error);
		})
	}
}

const done = () => {

	console.log(`### Total post count: ${totalPostCount} ###`.blue)
	console.log(`### Total char count: ${totalCharCount} ###`.blue)

	const runningTime = ((new Date).getTime() - startTimestamp) / 1000

	console.log(`### Completed in ${runningTime.toFixed(2)} sec, ${(totalPostCount / runningTime).toFixed(2)} post/sec`.blue)

	// try {
	// 	fs.mkdirSync(resolvePath(`./`))
	// } catch (e) {}

	// threads.forEach((thread, index) => {
	// 	saveThread(thread, index)
	// })
}

const init = () => {
	startTimestamp = (new Date).getTime()

	rp({
			uri: rootSubForum,
			transform: defaultTransform
		})
		.then(($) => {
			const match = rootSubForum.match(/f=(\d+)/)
			if (match.length !== 2) {
				console.error('Invalid forum url provided'.red)
				return
			}
			const forumId = parseInt(match[1], 10)

			const pageMatch = $('.pagenav').first().find('td').first().text().match(/od (\d+)/)

			let pageCount = 1

			if (pageMatch !== null && pageMatch.length === 2) {
				pageCount = Math.min(
					subForumPageLimit,
					parseInt(pageMatch[1], 10)
				)
			}

			// Fill the pages array
			for (let i = 1; i <= pageCount; i++) {
				pages.push({
					page: i,
					link: `http://www.forum.hr/forumdisplay.php?f=${forumId}&order=desc&page=${i}`
				})
			}

			console.log(`### Getting thread links ###`.blue)

			const pagesPromises = pages.map(page =>
				() => getThreads(page).then(threadsPart => threads = threads.concat(threadsPart))
			)

			promisePool(pagesPromises).then(() => {
				if (threads.length) {
					processThread(0)
				} else {
					done()
				}
			})
		})
		.catch(error => {
			console.error(error.red)
		})
}
